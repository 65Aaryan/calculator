//
// Created by kumar on 2/1/2021.
//

#ifndef CLCALCULATOR_CALCULATOR_H
#define CLCALCULATOR_CALCULATOR_H

class Calculator {
public:
    double calculate(double, double, char);
};


#endif //CLCALCULATOR_CALCULATOR_H
