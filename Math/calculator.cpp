//
// Created by kumar on 2/1/2021.
//

#include "calculator.h"
#include <stdexcept>


double Calculator::calculate(double op1, double op2, char oper) {
    switch(oper)
    {
        case '+':   return op1 + op2;   break;
        case '-':   return op1 - op2;   break;
        case '*':   return op1 * op2;   break;
        case '/':   if ( op2 == 0 ) throw std::runtime_error("divide by Zero!!");
                    return op1 / op2;   break;
        default:    throw std::runtime_error("Invalid Operator");

    }
}