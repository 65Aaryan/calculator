#include <iostream>
#include "Math/calculator.h"

int main()
{
    double x = 0.0, y = 0.0, result = 0.0;
    char oper;

    std::cout << " CI Calculator " << std::endl << "> ";

    Calculator calc;
    std::cin >> x >> oper >> y;
    try {
        result = calc.calculate(x, y, oper);
        std::cout << result;

    } catch (std::runtime_error &e) {
        std::cerr << e.what();
    }


    return 0;
}